//
//  ConsolesTableViewController.swift
//  MyGames
//
//  Created by Douglas Frari on 16/05/20.
//  Copyright © 2020 Douglas Frari. All rights reserved.
//

import UIKit
import CoreData

class ConsolesTableViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    // esse tipo de classe oferece mais recursos para monitorar os dados
    var fetchedResultController:NSFetchedResultsController<Console>!
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var label = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = UIColor(named: "second")
        appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.scrollEdgeAppearance = appearance
        self.navigationController?.navigationBar.compactAppearance = appearance
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        // mensagem default
        label.text = "Você não tem plataformas cadastradas"
        label.textAlignment = .center
        
        loadConsoles()
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
        AppDelegate.AppUtility.lockOrientation(.portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(.all)
    }

    @IBAction func telaAdicionarConsole(_ sender: Any) {
        let vc = ConsoleAddEditViewController(nibName: "ConsoleAddEditViewController", bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func loadConsoles() {
        let fetchRequest: NSFetchRequest<Console> = Console.fetchRequest()
        
        // definindo criterio da ordenacao de como os dados serao entregues
        let consoleNameSortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [consoleNameSortDescriptor]
        
        fetchedResultController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultController.delegate = self
        
        do {
            try fetchedResultController.performFetch()
        } catch  {
            print(error.localizedDescription)
        }
        
        //ConsolesManager.shared.loadConsoles(with: context)
        //tableView.reloadData()
    }
    
} // fim da classe

extension ConsolesTableViewController : UITableViewDelegate {
    
}

extension ConsolesTableViewController : UITableViewDataSource {
    
    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let count = fetchedResultController?.fetchedObjects?.count ?? 0
        
        tableView.backgroundView = count == 0 ? label : nil
        
        return count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "consoleCell", for: indexPath) as! ConsoleTableViewCell
        
        guard let console = fetchedResultController.fetchedObjects?[indexPath.row] else {
            return cell
        }
        
        cell.prepare(with: console)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let console =  fetchedResultController.fetchedObjects?[indexPath.row]
        
        let vc = ConsoleAddEditViewController(nibName: "ConsoleAddEditViewController", bundle: nil)
        vc.console = console
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    // Override to support editing the table view.
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            guard let console = fetchedResultController.fetchedObjects?[indexPath.row] else {
                print("Nao foi possivel obter a Plataforma da linha selecionada para deletar")
                return
            }
            context.delete(console) // foi escalado para ser deletado, mas precisamos confirmar com save
            
            do {
                try context.save()
                // efeito visual deletar poderia ser feito aqui, porem, faremos somente se o banco de dados
                //reagir informando que ocorreu uma mudanca (NSFetchedResultsControllerDelegate)
            } catch  {
                print(error.localizedDescription)
            }
        }
        
        //ConsolesManager.shared.deleteConsole(index: indexPath.row, context: context)
        //tableView.deleteRows(at: [indexPath], with: .fade)
    }
    
}

extension ConsolesTableViewController: NSFetchedResultsControllerDelegate {
    
    // sempre que algum objeto for modificado esse metodo sera notificado
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .delete:
            if let indexPath = indexPath {
                // Delete the row from the data source
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break
        default:
            tableView.reloadData()
        }
    }
}

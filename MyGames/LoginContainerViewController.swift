//
//  LoginContainerViewController.swift
//  MyGames
//
//  Created by aluno on 16/07/20.
//  Copyright © 2020 Douglas Frari. All rights reserved.
//

import UIKit

class LoginContainerModel {
    var myLogin: String
    var mySenha: String
    
    init(){
        self.myLogin = ""
        self.mySenha = ""
    }
}

class LoginContainerViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    var myModel = LoginContainerModel()
    
    private lazy var portraitViewController: UIViewController? = {
        return UIStoryboard(name: "LoginPortrait", bundle: nil).instantiateViewController(withIdentifier: "loginPortrait")
    }()

    private lazy var landscapeViewController: UIViewController? = {
        return UIStoryboard(name: "LoginLandscape", bundle: nil).instantiateInitialViewController()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigation()
        addChildController(child: portraitViewController)
        presentOnboarding()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        if size.height > size.width {
            addChildController(child: portraitViewController)
            
            if let vc = landscapeViewController as? LoginViewController {
                myModel.myLogin = vc.login.text!
                myModel.mySenha = vc.senha.text!
            }
            
            if let currentVc = portraitViewController as? LoginViewController {
                currentVc.configure(myModel)
            }
        } else {
            addChildController(child: landscapeViewController)
            
            if let vc = portraitViewController as? LoginViewController {
                myModel.myLogin = vc.login.text!
                myModel.mySenha = vc.senha.text!
            }
            
            if let currentVc = landscapeViewController as? LoginViewController {
                currentVc.configure(myModel)
            }
        }
    }

}

private extension LoginContainerViewController {
    
    func addChildController(child: UIViewController?) {
        guard let child = child else {
            return
        }

        child.view.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(child.view)
        configureChildConstraints(child: child.view)
    }

    func configureChildConstraints(child: UIView?) {
        child?.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        child?.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        child?.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        child?.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
    }

    func configureNavigation() {
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    func presentOnboarding() {
        navigationController?.present(OnboardingViewController(), animated: true)
    }
    
}

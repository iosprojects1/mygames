//
//  GameViewController.swift
//  MyGames
//
//  Created by Douglas Frari on 16/05/20.
//  Copyright © 2020 Douglas Frari. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {

    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbConsole: UILabel!
    @IBOutlet weak var lbReleaseDate: UILabel!
    @IBOutlet weak var ivCover: UIImageView!
    @IBOutlet weak var ivCoverConsole: UIImageView!
    
    var game: Game?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let button = UIBarButtonItem()
        button.title = "Editar"
        button.target = self
        button.action = #selector(adicionarGame)
        self.navigationItem.rightBarButtonItem = button
        self.navigationController?.navigationBar.tintColor = .white
    }
    
    @objc func adicionarGame(){
        let vc = GameAddEditViewController(nibName: "GameAddEditViewController", bundle: nil)
        vc.game = game
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        lbTitle.text = game?.title
        lbConsole.text = game?.console?.name
        if let releaseDate = game?.releaseDate {
            let formatter = DateFormatter()
            // forçando usar o formato de data do Brasil
            // Note: procure fazer da forma de obter automaticamente do sistema
            formatter.dateStyle = .long
            formatter.locale = Locale(identifier: "pt-BR")
            lbReleaseDate.text = "Lançamento: " + formatter.string(from: releaseDate)
        }
       
        if let image = game?.cover as? UIImage {
            ivCover.image = image
        } else {
            ivCover.image = UIImage(named: "noCoverFull")
        }
        
        if let imageConsole = game?.console?.cover as? UIImage {
            ivCoverConsole.image = imageConsole
        } else {
            ivCoverConsole.image = UIImage(named: "noCoverFull")
        }
    }

}

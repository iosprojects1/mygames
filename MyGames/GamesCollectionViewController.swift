//
//  GamesCollectionViewController.swift
//  MyGames
//
//  Created by aluno on 18/07/20.
//  Copyright © 2020 Douglas Frari. All rights reserved.
//

import UIKit
import CoreData

class GamesCollectionViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var fetchedResultController:NSFetchedResultsController<Game>!
    
    let notifyCollection = Notification.Name("atualizarCollection")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadGames()
      
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 170, height: 270)
        layout.sectionInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 25)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 10
        collectionView.collectionViewLayout = layout
        collectionView.register(GamesCollectionViewCell.nib(), forCellWithReuseIdentifier: "GamesCollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        // Do any additional setup after loading the view.
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(onReceiveData(_:)), name: notifyCollection, object: nil)
    }
    
    @objc func onReceiveData(_ notification:Notification) {
        collectionView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //NotificationCenter.default.removeObserver(self, name: notifyCollection, object: nil)
    }
    
    func loadGames() {
        let fetchRequest: NSFetchRequest<Game> = Game.fetchRequest()
        
        // definindo criterio da ordenacao de como os dados serao entregues
        let consoleNameSortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        fetchRequest.sortDescriptors = [consoleNameSortDescriptor]
        
        fetchedResultController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultController.delegate = self
        
        do {
            try fetchedResultController.performFetch()
        } catch  {
            print(error.localizedDescription)
        }
        
        //ConsolesManager.shared.loadConsoles(with: context)
        //tableView.reloadData()
    }

}

extension GamesCollectionViewController : UICollectionViewDelegate {
    
}

extension GamesCollectionViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return fetchedResultController?.fetchedObjects?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GamesCollectionViewCell", for: indexPath) as! GamesCollectionViewCell
                
        guard let game = fetchedResultController.fetchedObjects?[indexPath.row] else {
            return cell
        }

        cell.prepare(game)
        
        return cell
    }
    
}

extension GamesCollectionViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 170, height: 270)
    }
}

extension GamesCollectionViewController : NSFetchedResultsControllerDelegate {
    
}

//
//  GamesTableViewController.swift
//  MyGames
//
//  Created by Douglas Frari on 16/05/20.
//  Copyright © 2020 Douglas Frari. All rights reserved.
//

import UIKit
import CoreData

class GamesTableViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    // esse tipo de classe oferece mais recursos para monitorar os dados
    var fetchedResultController:NSFetchedResultsController<Game>!
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var label = UILabel()
    
    let notifyCollection = Notification.Name("atualizarTableview")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = UIColor(named: "main")
        appearance.shadowColor = nil
        appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        navigationController?.navigationBar.compactAppearance = appearance
        navigationController?.navigationBar.prefersLargeTitles = true
        
        // altera comportamento default que adicionava background escuro sobre a view principal
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.tintColor = .white
        searchController.searchBar.barTintColor = .white

        navigationItem.searchController = searchController

        // usando extensions
        searchController.searchBar.delegate = self
        searchController.searchResultsUpdater = self
        
        // mensagem default
        label.text = "Você não tem jogos cadastrados"
        label.textAlignment = .center
        
        loadGames()
        tableView.dataSource = self
        tableView.delegate = self
        //tableView.reloadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onReceiveData(_:)), name: notifyCollection, object: nil)
    }
    
    @objc func onReceiveData(_ notification:Notification) {
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppDelegate.AppUtility.lockOrientation(.portrait)
    }
      
    override func viewWillDisappear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(.all)
        //NotificationCenter.default.removeObserver(self, name: notifyCollection, object: nil)
    }
    
    // valor default evita precisar ser obrigado a passar o argumento quando chamado
    func loadGames(filtering: String = "") {
        
        let fetchRequest: NSFetchRequest<Game> = Game.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        if filtering.isEmpty == false {
            // COMO SE FOSSE WHERE do SQL
            // usando predicate: conjunto de regras para pesquisas
            // contains [c] = search insensitive (nao considera letras identicas)
            let predicate = NSPredicate(format: "title contains [c] %@", filtering)
            fetchRequest.predicate = predicate
        }
        
        // possui
        fetchedResultController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultController.delegate = self
        
        do {
            try fetchedResultController.performFetch()
        } catch  {
            print(error.localizedDescription)
        }
    }
    
    @IBAction func telaAdicionarGame(_ sender: Any) {
        let vc = GameAddEditViewController(nibName: "GameAddEditViewController", bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func exibirCollection(_ sender: Any) {
        let vc = UIStoryboard(name: "GamesCollection", bundle: nil).instantiateViewController(identifier: "collectionContainer")
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
} // fim da classe


extension GamesTableViewController : UITableViewDelegate {
    
}

extension GamesTableViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = fetchedResultController?.fetchedObjects?.count ?? 0
        
        tableView.backgroundView = count == 0 ? label : nil
        
        return count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! GameTableViewCell
        
        guard let game = fetchedResultController.fetchedObjects?[indexPath.row] else {
            return cell
        }
        
        cell.prepare(with: game)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let game =  fetchedResultController.fetchedObjects?[indexPath.row]
        
        let vc = GameViewController(nibName: "GameViewController", bundle: nil)
        vc.game = game
        navigationController?.pushViewController(vc, animated: true)
        
    }

    
    // Override to support editing the table view.
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            guard let game = fetchedResultController.fetchedObjects?[indexPath.row] else {
                print("Nao foi possivel obter o Game da linha selecionada para deletar")
                return
            }
            context.delete(game) // foi escalado para ser deletado, mas precisamos confirmar com save
            
            do {
                try context.save()
                // efeito visual deletar poderia ser feito aqui, porem, faremos somente se o banco de dados
                //reagir informando que ocorreu uma mudanca (NSFetchedResultsControllerDelegate)
            } catch  {
                print(error.localizedDescription)
            }
        }
    }
    
}

extension GamesTableViewController: NSFetchedResultsControllerDelegate {
    
    // sempre que algum objeto for modificado esse metodo sera notificado
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .delete:
            if let indexPath = indexPath {
                // Delete the row from the data source
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break
        default:
            tableView.reloadData()
        }
    }
}


extension GamesTableViewController: UISearchResultsUpdating, UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        // TODO
    }
   
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        loadGames()
        tableView.reloadData()
    }
   
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        loadGames(filtering: searchBar.text!)
        tableView.reloadData()
    }
}

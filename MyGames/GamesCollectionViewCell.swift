//
//  GamesCollectionViewCell.swift
//  MyGames
//
//  Created by aluno on 18/07/20.
//  Copyright © 2020 Douglas Frari. All rights reserved.
//

import UIKit

class GamesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var ivConsole: UIImageView!
    @IBOutlet weak var ivCover: UIImageView!
    @IBOutlet weak var container: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "GamesCollectionViewCell", bundle: nil)
    }
    
    func prepare(_ game: Game){
        container.layer.cornerRadius = 10
        container.clipsToBounds = true
        
        if let image = game.cover as? UIImage {
            ivCover.image = image
        } else {
            ivCover.image = UIImage(named: "noCover")
        }
        
        if let logo = game.console?.cover as? UIImage {
            ivConsole.image = logo
        } else {
            ivConsole.image = UIImage(named: "noCover")
        }
        
    }

}

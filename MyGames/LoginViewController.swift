//
//  LoginPortraitViewController.swift
//  MyGames
//
//  Created by aluno on 16/07/20.
//  Copyright © 2020 Douglas Frari. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var login: UITextField!
    @IBOutlet weak var senha: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func autenticar(_ sender: Any) {
        
        let vc = UIStoryboard(name: "Home", bundle: nil).instantiateInitialViewController()
        vc?.modalPresentationStyle = .fullScreen
        present(vc!, animated: true, completion: nil)
    }
    
    func configure(_ model: LoginContainerModel){
        login.text = model.myLogin
        senha.text = model.mySenha
    }

}
